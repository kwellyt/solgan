from code_cleaner import cleaning, masking, get_wordsets, get_enc_dec
from utils import auto_indent, path_check
import pandas as pd
from data_writer import Datawriter
from tqdm import tqdm


def preprocessing():
    print('Code cleaning...', end='  ')
    data = pd.read_pickle('./data/contract.pkl')
    data['clean_code'] = data['source_code'].map(cleaning)
    data['clean_code'] = data['clean_code'].map(lambda x: auto_indent(x, merge=False))
    data = data.loc[data['clean_code'].map(lambda x: len(x.split('\n'))) >= 10]
    clean_codes = data['clean_code']
    print('Datasets:', len(clean_codes))
    del data

    print('Code abstracting...')
    file = open('./data/abstracted_codes.txt', 'w')
    errors = 0
    for code in tqdm(clean_codes):
        code = masking(code)        
        try:
<<<<<<< HEAD
            file.write(code + '\t')
=======
            file.write(' '.join(code) + '\t')
>>>>>>> 47b359b84f4cd62365cd6616b7b05e8e13479da8
        except Exception as e:
            errors += 1
            pass
    print('Compile error samples:', errors)
    file.close()

    print('ENcoder/Decoder processing...')
    with open('./data/abstracted_codes.txt', 'r') as file:
        abstracted_codes = file.read().strip().split('\t')
        abstracted_codes = [[c for c in code.strip().split() if c != ''] for code in abstracted_codes]
    wordsets = get_wordsets(abstracted_codes)
    encoder, decoder = get_enc_dec(wordsets)
    pd.to_pickle(encoder, './data/encoder.pkl')
    pd.to_pickle(decoder, './data/decoder.pkl')
    return


if __name__ == '__main__':
    max_words = 20
    path_check('./data/')
    path_check('./data/tfrecord/')
    path_check('./data/tfrecord/train/')
    path_check('./data/tfrecord/test/')
    preprocessing()
    writer = Datawriter()
    writer.write(max_words)
