import tensorflow as tf


class Seq2Seq_attn:
    def __init__(self, layer_num, vocab_size, embedding_size, times, sos_id, eos_id,
                 batch_size=128, seq_length=20, num_units=128, learning_rate=0.0001):
        self.layer_num = layer_num
        self.batch_size = batch_size
        self.num_units = num_units
        self.seq_length = seq_length
        self.times = times
        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.lr = learning_rate
        self.SOS = sos_id
        self.EOS = eos_id

        # encoder
        self.enc_inputs = tf.placeholder(tf.int32, shape=[self.batch_size, self.seq_length * self.times], name="encoder_inputs")
        self.dropout = tf.placeholder(tf.float32, shape=None, name="dropout_rate")
        embedding_encoder = tf.get_variable("embedding_encoder", [self.vocab_size, self.embedding_size])
        encoder_emb_inputs = tf.nn.embedding_lookup(embedding_encoder, self.enc_inputs)
        encoder_cell = tf.nn.rnn_cell.MultiRNNCell([tf.nn.rnn_cell.LSTMCell(self.num_units) for _ in range(self.layer_num)])
        encoder_cell = tf.nn.rnn_cell.DropoutWrapper(encoder_cell, output_keep_prob=self.dropout)
        encoder_outputs, encoder_state = tf.nn.dynamic_rnn(encoder_cell, encoder_emb_inputs, dtype=tf.float32)

        # decoder
        self.dec_inputs = tf.placeholder(tf.int32, shape=[self.batch_size, self.seq_length], name="decoder_inputs")
        self.dec_length = tf.placeholder(tf.int32, shape=[self.batch_size], name="decoder_length")
        embedding_decoder = tf.get_variable("embedding_decoder", [self.vocab_size, self.embedding_size])
        decoder_emb_inputs = tf.nn.embedding_lookup(embedding_decoder, self.dec_inputs)

        projection_layer = tf.layers.Dense(self.vocab_size, use_bias=False)

        helper = tf.contrib.seq2seq.TrainingHelper(decoder_emb_inputs, self.dec_length)
        decoder_cell = tf.nn.rnn_cell.MultiRNNCell([tf.nn.rnn_cell.LSTMCell(self.num_units) for _ in range(self.layer_num)])
        decoder_cell = tf.nn.rnn_cell.DropoutWrapper(decoder_cell, output_keep_prob=self.dropout)
        attention_mechanism = tf.contrib.seq2seq.LuongAttention(self.num_units,
                                                                encoder_outputs,
                                                                memory_sequence_length=None)
        decoder_cell = tf.contrib.seq2seq.AttentionWrapper(decoder_cell,
                                                           attention_mechanism,
                                                           attention_layer_size=None)
        initial_state = decoder_cell.zero_state(self.batch_size, tf.float32).clone(cell_state=encoder_state)
        decoder = tf.contrib.seq2seq.BasicDecoder(decoder_cell,
                                                  helper,
                                                  initial_state,
                                                  output_layer=projection_layer)
        decoder_outputs, __, final_sequence_lengths = tf.contrib.seq2seq.dynamic_decode(decoder, impute_finished=True)
        max_decoder_length = tf.reduce_max(final_sequence_lengths)
        logits = decoder_outputs.rnn_output
        self.mutation = tf.argmax(logits, 2)

        # optimize
        self.dec_outputs = tf.placeholder(tf.int32, shape=[self.batch_size, self.seq_length], name='decoder_outputs')
        self.loss = tf.losses.sparse_softmax_cross_entropy(self.dec_outputs[:, :max_decoder_length], logits)

        global_step = tf.Variable(0, trainable=False)
        learning_rate = tf.train.exponential_decay(self.lr, global_step, 10000, 0.95, staircase=True)
        optimizer = tf.train.AdamOptimizer(learning_rate, epsilon=1e-5)
        params = tf.trainable_variables()
        gradients = tf.gradients(self.loss, params)
        clipped_gradients, __ = tf.clip_by_global_norm(gradients, 40.0)
        self.train_op = optimizer.apply_gradients(zip(clipped_gradients, params), global_step=global_step)

        # inference
        infer_helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(embedding_decoder,
                                                                tf.fill([self.batch_size], self.SOS),
                                                                self.EOS)
        infer_decoder = tf.contrib.seq2seq.BasicDecoder(decoder_cell,
                                                        infer_helper,
                                                        initial_state,
                                                        output_layer=projection_layer)
        infer_outputs, __, __ = tf.contrib.seq2seq.dynamic_decode(infer_decoder, maximum_iterations=self.seq_length)
        self.generation = infer_outputs.sample_id
        return

    def train(self, enc_input, dec_input, dec_output, dec_length):
        feed_dict = {self.enc_inputs: enc_input,
                     self.dec_inputs: dec_input,
                     self.dec_outputs: dec_output,
                     self.dec_length: dec_length,
                     self.dropout: 0.7}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.train_op], feed_dict=feed_dict)

    def mutate(self, enc_input, dec_input, dec_length):
        feed_dict = {self.enc_inputs: enc_input,
                     self.dec_inputs: dec_input,
                     self.dec_length: dec_length,
                     self.dropout: 1.0}
        sess = tf.get_default_session()
        return sess.run(self.mutation, feed_dict=feed_dict)

    def generate(self, enc_input):
        feed_dict = {self.enc_inputs: enc_input,
                     self.dropout: 1.0}
        sess = tf.get_default_session()
        return sess.run(self.generation, feed_dict=feed_dict)
