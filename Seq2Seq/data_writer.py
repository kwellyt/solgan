import pandas as pd
import numpy as np
from utils import path_check
import tensorflow as tf
from collections import deque


class Datawriter:
    def __init__(self):
        self.encoder = pd.read_pickle('./data/encoder.pkl')
        self.decoder = pd.read_pickle('./data/decoder.pkl')
        self.datasets = self._read()
        self.sample_size = len(self.datasets)
        self.START = self.encoder['<START>']
        self.END = self.encoder['<END>']
        self.PAD = self.encoder['<PAD>']
        self.SOS = self.encoder['<SOS>']
        self.EOS = self.encoder['<EOS>']
        self.filepath = './data/tfrecord'
        path_check(self.filepath)
        return

    def _read(self):
        with open('./data/masked_codes.txt', 'r') as file:
            masked_codes = file.read().strip().split('\t')
            masked_codes = [code for code in masked_codes if len(code) != 1]
            for i, code in enumerate(masked_codes):
                new = []
                for line in code.strip().split('<NEWLINE>'):
                    line = [self.encoder[d] for d in line.strip().split() if d != '']
                    new.append(line)
                masked_codes[i] = new
            return masked_codes

    def _mk_batch(self, lines, max_length=99, times=1):
        START_LINE = [self.START] + [self.PAD for __ in range(max_length)]
        END_LINE_in = [self.SOS] + [self.END] + [self.PAD for __ in range(max_length - 1)]
        END_LINE_out = [self.END] + [self.EOS] + [self.PAD for __ in range(max_length - 1)]
        START_LINE = np.array(START_LINE, dtype=np.int32)
        END_LINE_in = np.array(END_LINE_in, dtype=np.int32)
        END_LINE_out = np.array(END_LINE_out, dtype=np.int32)
        batch_enc_inputs = []
        batch_dec_inputs = []
        batch_dec_outputs = []
        if times > 1:
            enc_deque = deque()
            padding = np.array([self.PAD for __ in range(max_length + 1)], dtype=np.int32)
            for __ in range(times - 1):
                enc_deque.append(padding)
            enc_deque.append(START_LINE)
            batch_enc_inputs.append(np.concatenate(list(enc_deque)))
        else:
            batch_enc_inputs.append(START_LINE)
        for i in range(len(lines)):
            line = lines[i]
            length = len(line)
            if length <= max_length:
                enc_in = np.array(line + [self.PAD for __ in range(max_length - length + 1)], dtype=np.int32)
                dec_in = np.array([self.SOS] + line + [self.PAD for __ in range(max_length - length)], dtype=np.int32)
                dec_out = np.array(line + [self.EOS] + [self.PAD for __ in range(max_length - length)], dtype=np.int32)
                if times > 1:
                    enc_deque.append(enc_in)
                    enc_deque.popleft()
                    batch_enc_inputs.append(np.concatenate(list(enc_deque)))
                else:
                    batch_enc_inputs.append(enc_in)
                batch_dec_inputs.append(dec_in)
                batch_dec_outputs.append(dec_out)
        batch_dec_inputs.append(END_LINE_in)
        batch_dec_outputs.append(END_LINE_out)
        return batch_enc_inputs[::-1], batch_dec_inputs[::-1], batch_dec_outputs[::-1]

    def write(self, max_length, times=1):
        batch_enc_inputs = []
        batch_dec_inputs = []
        batch_dec_outputs = []
        for pointer in range(self.sample_size):
            lines = self.datasets[pointer]
            enc_in, dec_in, dec_out = self._mk_batch(lines, max_length, times)
            batch_enc_inputs.extend(enc_in)
            batch_dec_inputs.extend(dec_in)
            batch_dec_outputs.extend(dec_out)

        counter = 0
        while len(batch_enc_inputs) != 0:
            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            record_path = '%s/sequence%07d.tfrecord' % (self.filepath, counter)
            writer = tf.python_io.TFRecordWriter(record_path)
            sess.run(tf.global_variables_initializer())
            print(record_path)
            for _ in range(8096):
                try:
                    enc_in = batch_enc_inputs.pop()
                    dec_in = batch_dec_inputs.pop()
                    dec_out = batch_dec_outputs.pop()
                    feature = {'enc_inputs': self._bytes_feature(enc_in.tobytes()),
                               'dec_inputs': self._bytes_feature(dec_in.tobytes()),
                               'dec_outputs': self._bytes_feature(dec_out.tobytes())}
                    example = tf.train.Example(features=tf.train.Features(feature=feature))
                    writer.write(example.SerializeToString())
                    counter += 1
                except Exception as e:
                    print(e)
                    break
                finally:
                    sess.close()
            sess.close()
        return

    def _bytes_feature(self, value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


if __name__ == '__main__':
    max_words = 39
    times = 1
    writer = Datawriter()
    writer.write(max_words, times)
