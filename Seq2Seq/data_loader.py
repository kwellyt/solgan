import pandas as pd
import numpy as np
import tensorflow as tf
import glob


class Dataloader:
    def __init__(self, max_length):
        self.encoder = pd.read_pickle('./data/encoder.pkl')
        self.decoder = pd.read_pickle('./data/decoder.pkl')
        self.START = self.encoder['<START>']
        self.END = self.encoder['<END>']
        self.PAD = self.encoder['<PAD>']
        self.SOS = self.encoder['<SOS>']
        self.EOS = self.encoder['<EOS>']
        self.filepath = './data/tfrecord/'
        self.max_length = max_length
        return

    def initialize(self, batch_size, shuffle=True):
        self.sess = tf.get_default_session()
        self.next_element, self.dataset_init_op = self._loading(self.filepath, batch_size, shuffle)
        return

    def reset(self):
        self.sess.run(self.dataset_init_op)
        return

    def get_batch(self):
        enc_inputs, dec_inputs, dec_outputs = self.sess.run(self.next_element)
        dec_length = np.array([len(np.where(arr)[0]) for arr in dec_outputs], np.int32)
        return enc_inputs, dec_inputs, dec_outputs, dec_length

    def _decode_tfrecord(self, serialized_example):
        feature = {'enc_inputs': tf.FixedLenFeature([], tf.string),
                   'dec_inputs': tf.FixedLenFeature([], tf.string),
                   'dec_outputs': tf.FixedLenFeature([], tf.string)}
        features = tf.parse_single_example(serialized_example, features=feature)
        enc_inputs = tf.decode_raw(features['enc_inputs'], tf.int32)
        dec_inputs = tf.decode_raw(features['dec_inputs'], tf.int32)
        dec_outputs = tf.decode_raw(features['dec_outputs'], tf.int32)
        return enc_inputs, dec_inputs, dec_outputs

    def _loading(self, filepath, batch_size, shuffle=True):
        filenames = sorted(glob.glob(filepath + "*.tfrecord"))
        train_dataset = tf.data.TFRecordDataset(filenames)
        if shuffle:
            train_dataset = train_dataset.shuffle(10000)
        train_dataset = train_dataset.map(self._decode_tfrecord)
        train_dataset = train_dataset.batch(batch_size)
        iterator = train_dataset.make_initializable_iterator()
        next_element = iterator.get_next()
        dataset_init_op = iterator.initializer
        return next_element, dataset_init_op
