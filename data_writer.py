import pandas as pd
import numpy as np
from utils import path_check
import tensorflow as tf


class Datawriter:
    def __init__(self):
        self.encoder = pd.read_pickle('./data/encoder.pkl')
        self.decoder = pd.read_pickle('./data/decoder.pkl')
        self.datasets = self._read()
        self.sample_size = len(self.datasets)
        self.START = self.encoder['<START>']
        self.END = self.encoder['<END>']
        self.PAD = self.encoder['<PAD>']
        self.filepath = './data/tfrecord'
        path_check(self.filepath)
        return

    def _read(self):
        with open('./data/abstracted_codes.txt', 'r') as file:
            codes = file.read().strip().split('\t')
            codes = [c.strip().split() for c in codes]
            codes = [[c for c in code if c != ''] for code in codes]
            return codes

    def _mk_batch(self, lines, max_length=30):
        lines = [self.encoder[c] for c in lines]
        lines = [self.START] + lines + [self.END]
        sequence = []
        window_num = len(lines) - max_length + 1
        for i in range(window_num):
            line = lines[i:(max_length + i)]
            length = len(line)
            if length == max_length:
                sequence.append(np.array(line, dtype=np.int32))
            else:
                line = line + [self.PAD for __ in range(max_length - length)]
                sequence.append(np.array(line, dtype=np.int32))
        return sequence

    def write(self, max_length):
        counter = 0
        data_length = len(self.datasets)
        index = range(data_length)
        tr_index = index[:-100]
        te_index = index[-100:]

        # train datasets

        for idx in tr_index:
            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            record_path = '%s/train/sequence%07d.tfrecord' % (self.filepath, counter)
            writer = tf.python_io.TFRecordWriter(record_path)
            sess.run(tf.global_variables_initializer())
            print(record_path)
            sequences = self._mk_batch(self.datasets[idx], max_length)
            for seq in sequences:
                feature = {'sequence': self._bytes_feature(seq.tobytes())}
                example = tf.train.Example(features=tf.train.Features(feature=feature))
                writer.write(example.SerializeToString())
                counter += 1
            sess.close()

        # test datasets
        for idx in te_index:
            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            record_path = '%s/test/sequence%07d.tfrecord' % (self.filepath, counter)
            writer = tf.python_io.TFRecordWriter(record_path)
            sess.run(tf.global_variables_initializer())
            print(record_path)
            sequences = self._mk_batch(self.datasets[idx], max_length)
            for seq in sequences:
                feature = {'sequence': self._bytes_feature(seq.tobytes())}
                example = tf.train.Example(features=tf.train.Features(feature=feature))
                writer.write(example.SerializeToString())
                counter += 1
            sess.close()

    def _bytes_feature(self, value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
<<<<<<< HEAD

if __name__ == '__main__':
    max_words = 20
    writer = Datawriter()
    writer.write(max_words)


=======

if __name__ == '__main__':
    max_words = 20
    writer = Datawriter()
    writer.write(max_words)


>>>>>>> 47b359b84f4cd62365cd6616b7b05e8e13479da8
