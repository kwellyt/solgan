import pandas as pd
import tensorflow as tf
import glob


class Dataloader:
    def __init__(self, max_length):
        self.encoder = pd.read_pickle('./data/encoder.pkl')
        self.decoder = pd.read_pickle('./data/decoder.pkl')
        self.START = self.encoder['<START>']
        self.END = self.encoder['<END>']
        self.PAD = self.encoder['<PAD>']
        self.filepath = './data/tfrecord/train/'
        self.validpath = './data/tfrecord/test/'
        self.max_length = max_length
        return

    def initialize(self, batch_size, shuffle=True):
        self.sess = tf.get_default_session()
        self.next_element, self.dataset_init_op = self._loading(self.filepath, batch_size, shuffle)
        self.next_element_valid, self.dataset_init_op_valid = self._loading_valid(self.validpath)
        return

    def reset(self):
        self.sess.run([self.dataset_init_op, self.dataset_init_op_valid])
        return

    def get_batch(self):
        sequence = self.sess.run(self.next_element)
        return sequence

    def validset(self):
        sequence = self.sess.run(self.next_element_valid)
        return sequence

    def _decode_tfrecord(self, serialized_example):
        feature = {'sequence': tf.FixedLenFeature([], tf.string)}
        features = tf.parse_single_example(serialized_example, features=feature)
        sequence = tf.decode_raw(features['sequence'], tf.int32)
        return sequence

    def _loading(self, filepath, batch_size, shuffle=True):
        filenames = sorted(glob.glob(filepath + "*.tfrecord"))
        train_dataset = tf.data.TFRecordDataset(filenames)
        if shuffle:
            train_dataset = train_dataset.shuffle(10000)
        train_dataset = train_dataset.map(self._decode_tfrecord)
        train_dataset = train_dataset.batch(batch_size)
        iterator = train_dataset.make_initializable_iterator()
        next_element = iterator.get_next()
        dataset_init_op = iterator.initializer
        return next_element, dataset_init_op

    def _loading_valid(self, filepath):
        filenames = sorted(glob.glob(self.validpath + "*.tfrecord"))
        test_dataset = tf.data.TFRecordDataset(filenames)
        test_dataset = test_dataset.repeat(-1)
        test_dataset = test_dataset.shuffle(10000)
        test_dataset = test_dataset.map(self._decode_tfrecord)
        test_dataset = test_dataset.batch(10000)
        iterator = test_dataset.make_initializable_iterator()
        next_element = iterator.get_next()
        dataset_init_op = iterator.initializer
        return next_element, dataset_init_op
