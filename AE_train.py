from LaTextGAN.model import LSTMAutoencoder
from data_loader import Dataloader
from utils import path_check, bleu, raw
import argparse
import numpy as np
import os
import tensorflow as tf


def train():
    parser = argparse.ArgumentParser()
    parser.add_argument('--e', type=int, default=100, help='Embedding size')
    parser.add_argument('--s', type=int, default=20, help='Sequence length')
    parser.add_argument('--eh', type=int, default=100, help='Encoder hidden units size')
    parser.add_argument('--dh', type=int, default=600, help='Decoder hidden units size')
    parser.add_argument('--l', type=int, default=1, help='layer num')
    parser.add_argument('--lr', type=float, default=0.00005, help='Learning rate')
    parser.add_argument('--epoch', type=int, default=10, help='Epochs')
    parser.add_argument('--batch', type=int, default=1024, help='Batch size')
    parser.add_argument('--shuffle', type=bool, default=False, help='Shuffle')
    parser.add_argument('--reverse', type=bool, default=True, help='Reverse')
    parser.add_argument('--path', type=str, default='autoencoder', help='Model path')
    args = parser.parse_args()

    model_path = os.path.join('./models/', args.path)
    sample_path = './samples/'
    path_check('./models/')
    path_check('./logs/')
    path_check(sample_path)
    path_check(model_path)

    with tf.device("/cpu:0"):
        loader = Dataloader(args.s)
    # train
    with tf.Session() as sess:

        # Information
        with open('information.txt', 'w') as f:
            print('Embedding size:', args.e, file=f)
            print('Sequence length:', args.s, file=f)
            print('Encoder hidden units size:', args.eh, file=f)
            print('Decoder hidden units size:', args.dh, file=f)
            print('Number of layers:', args.l, file=f)
            print('Learning rate:', args.lr, file=f)
            print('Epochs:', args.epoch, file=f)
            print('Batch size:', args.batch, file=f)
            print('Shuffle:', args.shuffle, file=f)
            print('Reverse:', args.reverse, file=f)
            print('Model path:', model_path + '/', file=f)
            print('Sample path:', sample_path, file=f)
        print('Training start...')
        vocab_size = len(loader.encoder)
        model = LSTMAutoencoder(vocab_size=vocab_size,
                                layer_num=args.l,
                                embedding_size=args.e,
                                enc_num_units=args.eh,
                                dec_num_units=args.dh,
                                batch_size=args.batch,
                                seq_length=args.s,
                                reverse=args.reverse,
                                learning_rate=args.lr,
                                is_training=True)
        sess.run(tf.global_variables_initializer())
        loader.initialize(args.batch, shuffle=args.shuffle)
        saver = tf.train.Saver()
        writer = tf.summary.FileWriter('./log/', sess.graph)
        time_step = 0

        for epoch in range(1, args.epoch + 1):
            loader.reset()
            validset = loader.validset()
            valid_idx = np.random.choice(range(10000), 128)

            while True:
                try:
                    batch_seq = loader.get_batch()
                except tf.errors.OutOfRangeError:
                    break
                if batch_seq.shape[0] != args.batch:
                    break
                train_loss, __ = model.train(batch_seq)
                valid_loss, real, pred = model.valid(validset[valid_idx])
                score = bleu(real, pred, loader.decoder)
                result = ' '.join([loader.decoder[c] for c in pred[42][::-1]])
                arg = (epoch, time_step, train_loss, valid_loss, score, result[:79])
                print('Epoch: %d-%d Training... train: %.5f valid: %.5f score: %.5f %s   ' % arg, end='\r')
                time_step += 1

                if time_step % 1000 == 0:
                    valid_loss, real, pred = model.valid(validset)
                    score = bleu(real, pred, loader.decoder)
                    result = ' '.join([loader.decoder[c] for c in pred[42][::-1]])
                    arg = (epoch, time_step, train_loss, valid_loss, score, result[:79])
                    print('Epoch: %d-%d Training... train: %.5f valid: %.5f score: %.5f %s   ' % arg, end='\n')

                    # logging
                    summary = tf.Summary()
                    summary.value.add(tag='Train loss', simple_value=train_loss)
                    summary.value.add(tag='Valid_loss', simple_value=valid_loss)
                    writer.add_summary(summary, time_step)

            with open(sample_path + 'Real_%s.txt' % epoch, 'w') as f:
                f.write(raw(real, loader.decoder, args.reverse))

            with open(sample_path + 'Gen_%s.txt' % epoch, 'w') as f:
                f.write(raw(pred, loader.decoder, args.reverse))

            saver.save(sess, model_path + '/model', global_step=epoch)
    return


if __name__ == '__main__':
    train()
