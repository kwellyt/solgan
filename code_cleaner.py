import re
import pandas as pd
from nltk.tokenize import wordpunct_tokenize


#  Code cleaning process
def single_comments(code):
    single = code.split('\n')
    newsingle = []
    for line in single:
        line = line.strip()
        if line[:2] == '//':
            line = ''
        if 'http://' not in line and 'https://' not in line:
            line = re.sub(r'^//.*', '', line)
            line = re.sub(r'(?!:)//.*', '', line)
        if line != '':
            newsingle.append(line)
    return '\n'.join(newsingle)

def multi_comments(code):
    return re.sub(r'/[*].*?[*]/(?s)', '', code)

def cleaning(code):
    code = re.sub('\r', '', code)
    code = re.sub(r'pragma .+?;', '', code)
    code = single_comments(code)
    code = multi_comments(code)
    code = re.sub('\&nbsp\;', ' ', code)
    code = re.sub('\&lt\;', '<', code)
    code = re.sub('\&gt\;', '>', code)
    code = re.sub('\&amp\;', '&', code)
    code = re.sub(r'\;', ' ; ', code)
    code = re.sub(r'\:', ' ; ', code)
    code = re.sub(r'\(', ' ( ', code)
    code = re.sub(r'\)', ' ) ', code)
    code = re.sub(r'\[', ' [ ', code)
    code = re.sub(r'\]', ' ] ', code)
    code = re.sub(r'\{', ' { ', code)
    code = re.sub(r'\}', ' } ', code)    
    code = ' '.join(wordpunct_tokenize(code))
    code = re.sub(r'\}', ' }\n', code)
    code = re.sub(r'\;', ';\n', code)
    code = re.sub(r'\{', '{\n', code)
    return code


def matching(code, pattern_lst, replace, constraint=[]):
    sets = []
    lst = []
    for pattern in pattern_lst:
        lst.extend(re.findall(pattern, code))
    if lst:
        lst = pd.unique(lst)
        if len(constraint) >= 1:
            lst = [c for c in lst if c not in constraint]
        if replace == 'VAR':
            lst = [c for c in lst if 'VALUE' not in c]
        sets.extend([(lst[i], replace + str(i + 1)) for i in range(len(lst))])
    return sets


def literal_matching(code, pattern_lst, replace):
    exception = {c: '\\' + c for c in list('~!@#$%^&*(),./<>?[]{}\|=+')}
    sets = []
    for c in pattern_lst:
        sets.extend(re.findall(c, code))
    if sets:
        sets = [''.join([exception[c] if c in exception else c for c in list(s)]) for s in sets]
        sets = [(c, replace) for c in sets]
    return sets


def literal_pre_abstracting(code):
    # LiteralDefinition
    literal_lst = []
    hexa = [r'hex[\'][0-9a-fA-F]*[\']', r'hex[\"][0-9a-fA-F]*[\"]', r'0x[0-9a-fA-F]*(?=[ ]|[;]|[,])?']
    literal_lst.extend(literal_matching(code, hexa, 'HEXA'))
    string_literal = [r'[\'].*?[\']', r'[\"].*?[\"]']
    literal_lst.extend(literal_matching(code, string_literal, 'CHARCTR'))
    decimal_literal = [r'[0-9]+[ ][.][ ][0-9]*', r'[0-9]+[eE][0-9]+']
    literal_lst.extend(literal_matching(code, decimal_literal, 'DECIMAL'))
    integer_literal = [r'[ ][0-9]+[ ]']
    literal_lst.extend(literal_matching(code, integer_literal, ' INTEGER '))
    literal_lst = pd.unique(literal_lst)
    for p, r in literal_lst:
        code = re.sub(p, r, code)
    return code


def list_sub(code, matching_lst):
    for be, af in matching_lst:
        code = re.sub(r' %s ' % be, ' %s ' % af, code)
    return code


def array_abstracting(code):
    matching_lst = []
    enum = re.findall(r'enum [A-z0-9_]+? \{.*?\}', code, re.DOTALL)
    strct = re.findall(r'struct [A-z0-9_]+? \{.*?\}', code, re.DOTALL)

    for i, s in enumerate(strct):
        identifier = re.findall(r'struct ([A-z0-9_]+?) \{.*?\}', s, re.DOTALL)[0]
        values = re.findall(r'struct [A-z0-9_]+? \{(.*?)\}', s, re.DOTALL)[0].strip().split(';')
        values = [v.strip() for v in values if v != '']
        value_identifier = [v.split()[-1] for v in values]
        values_ch = [re.sub(' ' + v[-1], ' VALUE%s' % (n + 1), v[0])  for n, v in enumerate(zip(values, value_identifier))]
        s_ch = re.sub(identifier + ' ', 'STRUCT%s ' % (i + 1), s)
        for v, v_ch in zip(values, values_ch):
            s_ch = re.sub(v + ' ', v_ch + ' ', s_ch)
        exception = list('()[]{}')
        for e in exception:
            s = re.sub('\\' + e, '\\' + e, s)
        matching_lst.append((s, s_ch))
        matching_lst.append((identifier + ' ',  'STRUCT%s ' % (i + 1)))    
        matching_lst.extend([('%s . %s ' % (identifier, v), 'STRUCT%s . VALUE%s ' % (i + 1, n + 1)) for n, v in enumerate(value_identifier)])
        
    for i, s in enumerate(enum):
        identifier = re.findall(r'enum ([A-z0-9_]+?) \{.*?\}', s, re.DOTALL)[0]
        values = re.findall(r'enum [A-z0-9_]+? \{(.*?)\}', s, re.DOTALL)[0].strip().split(',')
        values = [v.strip() for v in values if v != '']
        value_identifier = [v.split()[-1] for v in values]
        values_ch = [' ' + re.sub(v[-1], 'VALUE%s ' % (n + 1), v[0])  for n, v in enumerate(zip(values, value_identifier))]
        s_ch = re.sub(identifier + ' ', 'ENUM%s ' % (i + 1), s)
        for v, v_ch in zip(values, values_ch):
            s_ch = re.sub(v + ' ', v_ch + ' ', s_ch)
        exception = list('()[]{}')
        for e in exception:
            s = re.sub('\\' + e, '\\' + e, s)
        matching_lst.append((s, s_ch))
        matching_lst.append((identifier + ' ',  'ENUM%s ' % (i + 1)))    
        matching_lst.extend([('%s . %s ' % (identifier, v), 'ENUM%s . VALUE%s ' % (i + 1, n + 1)) for n, v in enumerate(value_identifier)])

    if matching_lst:
        for mat in matching_lst:
            code = re.sub(mat[0], mat[1], code)
    return code


def var_types():
    address = ['address']
    boolen = ['bool']
    string = ['string']
    fixed = ['fixed[0-9]+x[0-9]']
    ufixed = ['ufixed[0-9]+x[0-9]']
    var = ['var']
    Int = ['int', 'int8', 'int16', 'int24', 'int32', 'int40', 'int48', 'int56', 'int64', 'int72',
           'int80', 'int88', 'int96', 'int104', 'int112', 'int120', 'int128', 'int136', 'int144', 'int152', 'int160',
           'int168', 'int176', 'int184', 'int192', 'int200', 'int208', 'int216', 'int224', 'int232',
           'int240', 'int248', 'int256']
    Uint = ['uint', 'uint8', 'uint16', 'uint24', 'uint32', 'uint40', 'uint48', 'uint56', 'uint64',
            'uint72', 'uint80', 'uint88', 'uint96', 'uint104', 'uint112', 'uint120', 'uint128', 'uint136', 'uint144',
            'uint152', 'uint160', 'uint168', 'uint176', 'uint184', 'uint192', 'uint200', 'uint208', 'uint216',
            'uint224', 'uint232', 'uint240', 'uint248', 'uint256']
    Byte = ['byte', 'bytes', 'bytes1', 'bytes2', 'bytes3', 'bytes4', 'bytes5', 'bytes6', 'bytes7',
            'bytes8', 'bytes9', 'bytes10', 'bytes11', 'bytes12', 'bytes13', 'bytes14', 'bytes15', 'bytes16', 'bytes17',
            'bytes18', 'bytes19', 'bytes20', 'bytes21', 'bytes22', 'bytes23', 'bytes24', 'bytes25', 'bytes26',
            'bytes27', 'bytes28', 'bytes29', 'bytes30', 'bytes31', 'bytes32']

    ElementaryTypeName = address + boolen + string + fixed + ufixed + var + Int + Uint + Byte

    UserDefinedTypeName = ['STRUCT[0-9]+', 'ENUM[0-9]+', 'CONTRACT[0-9]+', 'MAPPING[0-9]+', 'CONTRACT[0-9]+', 'LET[0-9]+']
    
    Mapping = ['mapping \( [A-z0-9_]+? => .+ \)']
    
    let = [r'let ([A-z0-9_]+?) \:\=']

    TypeName = ElementaryTypeName + UserDefinedTypeName + Mapping
    ArrayTypeName = [t + ' \[ .*? \]' for t in TypeName]
    TypeName = TypeName + ArrayTypeName

    second = ['', 'public ', 'internal ', 'private ', 'constant ', 'memory ', 'storage ', 'indexed ']
    third = ['', 'public ', 'internal ', 'private ', 'constant ', 'memory ', 'storage ', 'indexed ']

    constraint = ['public', 'internal', 'private', 'constant', 'memory', 'storage', 'indexed', '[', 'is', 'for']

    pattern_lst = []
    for f in TypeName:
        for s in second:
            for t in third:
                pattern_lst.append(r'%s %s%s([A-z0-9_]+?) ' % (f, s, t))
    pattern_lst = pattern_lst + let
    return pattern_lst, constraint



def abstracting(code):    
    matching_lst = []
    func = [r'function ([A-z0-9\_]+) \(.*?\)']
    modif = [r'modifier ([A-z0-9\_]+) \(.*?\)']
    event = [r'event ([A-z0-9\_]+) \(.*?\)']
    function = func + modif + event
    code = list_sub(code, matching(code, function, 'FUNCTION'))

    contract = [r'contract ([A-z0-9\_]+).*? \{']
    library = [r'library ([A-z0-9\_]+).*? \{']
    interface = [r'interface ([A-z0-9\_]+).*? \{']
    contract = contract + library + interface
    code = list_sub(code, matching(code, contract, 'CONTRACT'))

    type_name, constraint = var_types()
    code = list_sub(code, matching(code, type_name, 'VAR', constraint))
    return code


def masking(code):
    code = literal_pre_abstracting(code)
    code = array_abstracting(code)
    code = abstracting(code)
    code = code.split()
    code = ' '.join(code)
    return code


# Convert code to trainable data process
def get_wordsets(dfm):
    bag = set()
    for c in dfm:
        bag = bag | set(c)
    print('Total words:', len(bag))
    wordsets = sorted(list(bag))
    wordsets = ['<PAD>', '<START>', '<END>'] + wordsets
    return wordsets


def get_enc_dec(wordsets):
    encoder = {w: i for i, w in enumerate(wordsets)}
    decoder = {i: w for i, w in enumerate(wordsets)}
    return encoder, decoder
