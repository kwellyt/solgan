from model import Seq2Seq_attn
from data_loader import Dataloader
from utils import path_check, generate, mutate, raw
import argparse
import os
import tensorflow as tf


def test():
    parser = argparse.ArgumentParser()
    parser.add_argument('--t', type=int, default=5, help='Encoder memory times')
    parser.add_argument('--e', type=int, default=100, help='Embedding size')
    parser.add_argument('--s', type=int, default=30, help='Sequence length')
    parser.add_argument('--h', type=int, default=256, help='Hidden units size')
    parser.add_argument('--l', type=int, default=2, help='layer num')
    parser.add_argument('--lr', type=float, default=0.0001, help='Learning rate')
    parser.add_argument('--epoch', type=int, default=50, help='Epochs for gen')
    parser.add_argument('--batch', type=int, default=256, help='Batch size')
    parser.add_argument('--path', type=str, default='seq2seq', help='Model path')
    args = parser.parse_args()

    model_path = os.path.join('./model/', args.path)
    sample_path = os.path.join('./test')
    path_check('./model/')
    path_check('./log/')
    path_check(sample_path)
    path_check(model_path)

    with tf.device("/cpu:0"):
        loader = Dataloader(args.h)

    # test
    with tf.Session() as sess:
        print('Generating start...')
        vocab_size = len(loader.encoder)
        model = Seq2Seq_attn(layer_num=args.l,
                             vocab_size=vocab_size,
                             embedding_size=args.e,
                             times=args.t,
                             sos_id=loader.SOS,
                             eos_id=loader.EOS,
                             batch_size=args.batch,
                             seq_length=args.s,
                             num_units=args.h,
                             learning_rate=args.lr)
        sess.run(tf.global_variables_initializer())
        loader.initialize(args.batch, shuffle=False)
        ckpt = tf.train.get_checkpoint_state(model_path)
        saver = tf.train.Saver()
        saver.restore(sess, ckpt.model_checkpoint_path)
        loader.reset()

        for epoch in range(1, args.epoch + 1):
            enc_inputs, dec_inputs, dec_outputs, dec_length = loader.get_batch()

            # epoch logging
            with open(sample_path + '/raw%d.txt' % epoch, 'w') as sample:
                gen = raw(dec_inputs, loader.decoder)
                sample.write(gen)
                print(sample)
            with open(sample_path + '/mutation%d.txt' % epoch, 'w') as sample:
                gen = mutate(model.mutate, enc_inputs, dec_inputs, dec_length, loader.decoder)
                sample.write(gen)
                print(sample)
            with open(sample_path + '/generation%d.txt' % epoch, 'w') as sample:
                gen = generate(model.generate, loader.encoder, loader.decoder,
                               args.s, model.batch_size, args.t, min(200, args.batch))
                sample.write(gen)
                print(sample)
    return


if __name__ == '__main__':
    test()
