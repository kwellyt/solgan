from gensim.models import Word2Vec
from utils import path_check
import re


with open('./data/masked_codes.txt', 'r') as file:
    codes = file.read().strip().split('\t')
    codes = [code for code in codes if len(code) != 1]
    new = []
    for code in codes:
        code = re.sub('<NEWLINE>', '\n', code)
        code = code.split()
        code = ['<START>'] + code + ['<END>']
        new.append(code)
    codes = new
    del new

model = Word2Vec(codes, size=128, window=5, min_count=1, workers=4)
path_check('./model/word2vec/')
model.save("./model/word2vec/word2vec.mdl")
