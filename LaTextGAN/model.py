import tensorflow as tf


class LSTMAutoencoder:
    def __init__(self, vocab_size, layer_num=1, embedding_size=128, enc_num_units=128, batch_size=128,
                 dec_num_units=512, seq_length=30, reverse=True, learning_rate=0.0005, is_training=True):
        self.layer_num = layer_num
        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.enc_num_units = enc_num_units
        self.dec_num_units = dec_num_units
        self.seq_length = seq_length
        self.batch_size = batch_size
        self.lr = learning_rate
        self.is_training = is_training
        self.cell = tf.nn.rnn_cell.LSTMCell

        self.enc_inputs = tf.placeholder(tf.int32, shape=[None, self.seq_length], name="encoder_inputs")
        self.dropout = tf.placeholder(tf.float32, shape=None, name="dropout_rate")

        with tf.variable_scope('Encoder', reuse=tf.AUTO_REUSE):
            self.encoder_outputs, encoder_state = self.encoder()

            if self.is_training:
                self.dec_inputs = self.encoder_outputs
            else:
                self.dec_inputs = tf.placeholder(tf.float32, shape=[None, self.seq_length, self.enc_num_units], name="decoder_inputs")

        with tf.variable_scope('Decoder', reuse=tf.AUTO_REUSE):
            decoder_outputs, decoder_state = self.decoder()
            if reverse:
                self.targets = tf.transpose(self.enc_inputs, [1, 0])
                self.targets = self.targets[::-1]
                self.targets = tf.transpose(self.targets, [1, 0])
            else:
                self.targets = self.enc_inputs
            self.decoder_outputs = tf.layers.dense(decoder_outputs,
                                                   self.vocab_size,
                                                   kernel_initializer=tf.glorot_normal_initializer())
        self.sequences = tf.argmax(self.decoder_outputs, 2)
        self.loss = tf.losses.sparse_softmax_cross_entropy(self.targets, self.decoder_outputs)
        global_step = tf.Variable(0, trainable=False)
        learning_rate = tf.train.exponential_decay(self.lr, global_step, 100000, 0.95)
        optimizer = tf.train.AdamOptimizer(learning_rate, epsilon=1e-5)
        params = tf.trainable_variables()
        gradients = tf.gradients(self.loss, params)
        clipped_gradients, __ = tf.clip_by_global_norm(gradients, 40.0)
        self.train_op = optimizer.apply_gradients(zip(clipped_gradients, params), global_step=global_step)
        return

    def encoder(self):
        embedding_encoder = tf.get_variable("embedding_encoder", [self.vocab_size, self.embedding_size])
        encoder_emb_inputs = tf.nn.embedding_lookup(embedding_encoder, self.enc_inputs)
        encoder_cell = tf.nn.rnn_cell.MultiRNNCell([self.cell(self.enc_num_units) for _ in range(self.layer_num)])
        encoder_outputs, encoder_state = tf.nn.dynamic_rnn(encoder_cell, encoder_emb_inputs, dtype=tf.float32)
        return encoder_outputs, encoder_state

    def decoder(self):
        decoder_cell = tf.nn.rnn_cell.MultiRNNCell([self.cell(self.dec_num_units) for _ in range(self.layer_num)])
        decoder_outputs, decoder_state = tf.nn.dynamic_rnn(decoder_cell, tf.layers.dropout(self.dec_inputs, self.dropout), dtype=tf.float32)
        return decoder_outputs, decoder_state

    def train(self, enc_inputs):
        feed_dict = {self.enc_inputs: enc_inputs,
                     self.dropout: 0.7}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.train_op], feed_dict=feed_dict)

    def valid(self, validset):
        feed_dict = {self.enc_inputs: validset,
                     self.dropout: 1.0}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.targets, self.sequences], feed_dict=feed_dict)

    def representation(self, enc_inputs, dec_inputs):
        feed_dict = {self.enc_inputs: enc_inputs,
                     self.dropout: 1.0}
        sess = tf.get_default_session()
        return sess.run(self.encoder_outputs, feed_dict=feed_dict)

    def decode(self, dec_inputs):
        feed_dict = {self.dec_inputs: dec_inputs,
                     self.dropout: 1.0}
        sess = tf.get_default_session()
        return sess.run(self.decoder_outputs, feed_dict=feed_dict)
