import json
import glob
import pandas as pd
import numpy as np
from pyprind import ProgBar
import os
import timeit
import re
from nltk.translate.bleu_score import corpus_bleu, SmoothingFunction
from collections import deque


def path_check(path):
    if os.path.exists(path) is False:
        os.mkdir(path)
    return


def runtime(func):
    def running(*args, **kwargs):
        start = timeit.default_timer()
        data = func(*args, **kwargs)
        end = timeit.default_timer()
        print(round((end - start) / 60, 2), 'minutes')
        return data
    return running


def datasets():
    filepath = glob.glob('./data/json/*')
    bar = ProgBar(len(filepath), title='Loading...')
    datasets = pd.DataFrame(columns=['address', 'contract_name', 'source_code', 'byte_code'])
    for i, path in enumerate(filepath):
        with open(path) as f:
            f = f.read()
            data = json.loads(f)
            datasets = datasets.append(pd.DataFrame(data, index=[i]), sort=False)
        bar.update()
    return datasets


def auto_merge(code):
    code = code.split()
    new_code = []
    bag = []
    for token in code:
        if len(token) == 1 and token.isalpha():
            bag.append(token)
        else:
            if bag:
                new_code.append(''.join(bag))
                bag = []
            new_code.append(token)
    if bag:
        new_code.append(''.join(bag))
    code = ' '.join(new_code)
    return code


def auto_indent(code, merge=True):
    lines = code.split('\n')
    new_lines = []
    indent_step = 0
    for line in lines:
        line = line.strip()
        line = ' '.join(line.split())
        if line != '':
            line = re.sub('[ ]+', ' ', line)
            if merge:
                line = auto_merge(line)
            if re.search('}', line):
                indent_step -= 1
            line = ''.join(['  '] * indent_step) + line
            new_lines.append(line)
            if re.search('{', line):
                indent_step += 1
    return'\n'.join(new_lines)


def code_restore(code):
    code = ' '.join(code)
    code = re.sub('<START>', '', code)
    code = re.sub('<EOS>', '', code)
    code = re.sub('<PAD>', '', code)
    code = re.sub('<END>', '', code)
    code = re.sub('<NEWLINE>', '\n', code)
    code = auto_indent(code)
    return code


def generate(gen_op, encoder, decoder, seq_length, batch_size, times, maximum_length=200):
    START = encoder['<START>']
    END = encoder['<END>']
    gen_out = np.zeros((batch_size, seq_length))
    gen_in = deque()
    for __ in range(times):
        gen_in.append(np.zeros(seq_length))
    gen_in[-1][0] = START
    lines = np.tile(np.concatenate(list(gen_in)), (batch_size, 1))

    for i in range(maximum_length):
        temp = list(gen_op(lines)[i])
        if len(temp) < seq_length:
            temp = temp + [0 for _ in range(seq_length - len(temp))]
        gen_out[i] = np.array(temp)
        gen_in.append(np.array(temp))
        gen_in.popleft()
        if END in temp:
            break
        else:
            lines = np.tile(np.concatenate(list(gen_in)), (batch_size, 1))
    source_code = '\n'.join([code_restore([decoder[i] for i in line]) for line in gen_out])
    source_code = auto_indent(source_code)
    return source_code


def raw(enc_inputs, decoder, reverse=False):
    if reverse:
        source_code = '\n'.join([code_restore([decoder[i] for i in line[::-1]]) for line in enc_inputs])
    else:
        source_code = '\n'.join([code_restore([decoder[i] for i in line]) for line in enc_inputs])
    source_code = auto_indent(source_code)
    return source_code


def mutate(gen_op, enc_inputs, dec_inputs, dec_length, decoder):
    gen = gen_op(enc_inputs, dec_inputs, dec_length)
    source_code = '\n'.join([code_restore([decoder[i] for i in line]) for line in gen])
    source_code = auto_indent(source_code)
    return source_code


def bleu(real, gen, decoder):
    chencherry = SmoothingFunction()
    real = [[decoder[i] for i in line if decoder[i]] for line in real]
    gen = [[decoder[i] for i in line if decoder[i]] for line in gen]
    score = corpus_bleu(real, gen, smoothing_function=chencherry.method1)
    return score


def print_restore(code):
    # restore
    new_code = [c for c in code if c != '<WHITESPACE>']
    new_code = ['    ' if c == '<INDENT>' else c for c in new_code]
    new_code = ['\n' if c == '<NEWLINE>' else c for c in new_code]
    new_code = ' '.join(new_code)
    print(new_code)
    return
